import App from '../App'

export default [{
    hashbang: false,
    history: true,
    component: App,
}]