import Vue from 'vue'
import Router from 'vue-router'
const About = resolve => require(['../components/About'], resolve);
const Home = resolve => require(['../components/Home'], resolve);

Vue.use(Router)


const router = new Router({
    mode: 'hash',
    routes: [
        { path: '/', component: Home },
        { path: '/about', component: About },
        { path: '/home', component: Home }
      ]
})

export default router